<?php
class Kategori_news_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }
    
    public function record_count()
    {
        return $this->db->count_all('kategori_news');
    }
    
    public function get_kategori_news($slug = FALSE)
    {
        // if ($slug === FALSE)
        // {
        //     $query = $this->db->get('kategori_news', array('user_id' => $this->session->userdata('user_id')));
        //     return $query->result_array(); // $query->result(); // returns object
        // }
        return $this->db->get('kategori_news');

        // $query = $this->db->get_where('news', array('slug' => $slug, 'user_id' => $this->session->userdata('user_id')));
        // return $query->row_array(); // $query->row(); // returns object
        
        // $query->num_rows(); // returns number of rows selected, similar to counting rows
        // $query->num_fields(); // returns number of fields selected
    }
        
    // public function get_news_by_id($id = 0)
    // {
    //     if ($id === 0)
    //     {
    //         $query = $this->db->get('news');
    //         return $query->result_array();
    //     }
 
    //     $query = $this->db->get_where('news', array('id' => $id));
    //     return $query->row_array();
    // }
    
    public function set_kategori_news($id = 0)
    {
        $this->load->helper('url');
 
        $slug = url_title($this->input->post('title'), 'dash', TRUE);
 
        $data = array(
            'name' => $this->input->post('name'), 
        );
        
        if ($id == 0) {
            //$this->db->query('YOUR QUERY HERE');
            return $this->db->insert('kategori_news', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('kategori_news', $data);
        }
    }
    
    public function delete_news($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('kategori_news'); // $this->db->delete('news', array('id' => $id));  
        
        // error() method will return an array containing its code and message
        // $this->db->error();
    }
}
