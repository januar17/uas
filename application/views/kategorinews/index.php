<h2><?php echo $title; ?></h2>
 
<table border='1' cellpadding='4'>
    <tr>
        <td><strong>id</strong></td>
        <td><strong>Name</strong></td>
    </tr>
<?php foreach ($kategori_news as $kategori_news_item): ?>
        <tr>
            <td><?php echo $kategori_news_item['id']; ?></td>
            <td><?php echo $kategori_news_item['name']; ?></td>
            <td>
              
                <?php if ($this->session->userdata('is_logged_in')) { ?>
                | 
                <a href="<?php echo site_url('kategori_news/edit/'.$news_item['id']); ?>">Edit</a> | 
                <a href="<?php echo site_url('kategori_news/delete/'.$news_item['id']); ?>" onClick="return confirm('Are you sure you want to delete?')">Delete</a>
                <?php } // end if ?>
                
            </td>
        </tr>
<?php endforeach; ?>
</table>
